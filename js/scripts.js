/*Hamburger Menu*/
$( '#hamburger' ).click(function() {
    $( '.header-right' ).toggleClass( 'visible' );
});
/*End Hamburger Menu*/
/*Dropdown Menu*/
$('.dropdown').click(function () {
    $(this).attr('tabindex', 1).focus();
    $(this).toggleClass('active');
    $(this).find('.dropdown-menu').slideToggle(300);
});
$('.dropdown').focusout(function () {
    $(this).removeClass('active');
    $(this).find('.dropdown-menu').slideUp(300);
});
$('.dropdown .dropdown-menu li').click(function () {
    $(this).parents('.dropdown').find('span').text($(this).text());
    $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
});
/*End Dropdown Menu*/
/*Range slider count*/

$('.range-slider').change( function() {
        $( '.range-slider-count' ).text( $(this).val() );
    })
    .change();
/*End range slider count*/